## CONTENTS OF THIS FILE

* Introduction
* Requirements
* Installation
* Configuration


## INTRODUCTION

This module adds a new reference method called "Taxonomy terms with role filter"
that can be selected when configuring a taxonomy term reference field. When
selected, the available terms in the reference field will be filtered based on
the current user's role via a field configured on the vocabulary that is being
referenced.

For this filter to work, the target vocabulary must have a "Role"
reference field. Then, on each term, content editors can specify which user
roles should be able to select the term. If no role is selected, then a user
with any role can select the term.

## REQUIREMENTS

This module requires the following core module:

 * Taxonomy

This module requires a "Role" reference field on the target vocabulary. To
establish this, create a field with the "Other" reference type, then choose
"Role" as the type of item to reference in the field settings.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules


## CONFIGURATION

 1. Enable the module.

 2. Ensure the target vocabulary has a "Role" reference field.

 3. When setting up the taxonomy term reference field, choose the "Taxonomy
    terms with role filter" selection method.
