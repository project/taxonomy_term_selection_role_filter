<?php

namespace Drupal\taxonomy_term_selection_role_filter\Plugin\EntityReferenceSelection;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Plugin\EntityReferenceSelection\TermSelection;

/**
 * Plugin description.
 *
 * @EntityReferenceSelection(
 *   id = "taxonomy_term_selection_role_filter",
 *   label = @Translation("Taxonomy terms with role filter"),
 *   group = "taxonomy_term_selection_role_filter",
 *   entity_types = {"taxonomy_term"},
 *   weight = 0
 * )
 */
class TaxonomyTermRoleFilterSelection extends TermSelection {

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    if ($match || $limit) {
      return parent::getReferenceableEntities($match, $match_operator, $limit);
    }

    $options = [];

    $bundles = $this->entityTypeBundleInfo->getBundleInfo('taxonomy_term');
    $bundle_names = $this->getConfiguration()['target_bundles'] ?: array_keys($bundles);

    $has_admin_access = $this->currentUser->hasPermission('administer taxonomy');
    $current_user_roles = $this->currentUser->getRoles(TRUE);
    $unpublished_terms = [];

    $configuration = $this->getConfiguration();
    /** @var \Drupal\node\Entity\Node $node */
    $node = $configuration['entity'];

    $existing_terms = $this->getExistingTerms($node, $bundles);

    /** @var \Drupal\Taxonomy\VocabularyStorage $vocabulary_storage */
    $vocabulary_storage = $this->entityTypeManager->getStorage('taxonomy_vocabulary');

    /** @var \Drupal\taxonomy\TermStorage $term_storage */
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    // Remove terms that are not allowed to be used by someone with the current
    // user's role. Include any existing terms saved in this field, regardless.
    // If the user is an administrator, include all terms.
    foreach ($bundle_names as $bundle) {
      if ($vocabulary = $vocabulary_storage->load($bundle)) {

        $role_fields = $this->getUserRoleFields($bundle);

        /** @var \Drupal\taxonomy\TermInterface[] $terms */
        if ($terms = $term_storage->loadTree($vocabulary->id(), 0, NULL, TRUE)) {
          foreach ($terms as $term) {
            if (!$has_admin_access && (!$term->isPublished() || in_array($term->parent->target_id, $unpublished_terms))) {
              $unpublished_terms[] = $term->id();
              continue;
            }

            if ($has_admin_access || in_array($term->id(), $existing_terms)) {
              $this->addTermToOptions($options, $term, $vocabulary);
            }
            else {
              foreach ($role_fields as $field) {
                $term_roles = $term->get($field->getName())->getValue();

                if (empty($term_roles)) {
                  $this->addTermToOptions($options, $term, $vocabulary);
                }
                else {
                  foreach ($term_roles as $role) {
                    if (in_array($role['target_id'], $current_user_roles)) {
                      $this->addTermToOptions($options, $term, $vocabulary);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Display information about the configured user role reference fields on
    // each taxonomy that can be referenced.
    $target_bundles = $form_state->getFormObject()->getEntity()->getSettings()['handler_settings']['target_bundles'];
    $fields = [];
    foreach ($target_bundles as $bundle) {
      $fields = array_merge($fields, $this->getUserRoleFields($bundle));
    }
    $display_values = $this->formatFieldsForDisplay($fields);

    $form['third_party_settings']['taxonomy_term_role_filter']['user_role_fields_display'] = [
      '#type' => 'item',
      '#title' => t('Role fields'),
      '#description' => t('<p>The following fields will be used to determine which terms are available for selection from each vocabulary: <ul>@fields</ul></p><p>If other vocabularies are allowed to be referenced and there is not a user role reference field configured on that vocabulary, then all terms will be available for selection.</p>', [
        '@fields' => $display_values,
      ]),
    ];
    return $form;
  }

  /**
   * Helper function to format a term in the options array.
   *
   * @param array &$options
   *   The referenced options array.
   * @param \Drupal\taxonomy\Entity\Term $term
   *   The taxonomy term to add to the options array.
   * @param \Drupal\taxonomy\Entity\Vocabulary $vocabulary
   *   The taxonomy vocabulary the term belongs to.
   */
  protected function addTermToOptions(array &$options, Term $term, Vocabulary $vocabulary) {
    $options[$vocabulary->id()][$term->id()] = str_repeat('-', $term->depth) . Html::escape($this->entityRepository->getTranslationFromContext($term)->label());
  }

  /**
   * Given a vocabulary, find any user role reference fields.
   *
   * @param string $bundle
   *   The bundle to search for user role reference fields.
   *
   * @return array
   *   The list of user role reference fields.
   */
  protected function getUserRoleFields($bundle) {
    // Get the all fields except for base fields for the vocabulary.
    $fields = [];
    $bundle_fields = $this->entityFieldManager->getFieldDefinitions('taxonomy_term', $bundle);
    $base_fields = $this->entityFieldManager->getBaseFieldDefinitions('taxonomy_term', $bundle);
    $fields = array_merge($fields, array_diff_key($bundle_fields, $base_fields));

    // Remove fields that aren't user role entity reference fields.
    $fields = array_filter($fields, function ($field) {
      return $field->getType() === 'entity_reference' && $field->getFieldStorageDefinition()->getSettings()['target_type'] === 'user_role';
    });

    return $fields;
  }

  /**
   * Collect existing values from fields that use this filter.
   *
   * Given a node and vocabulary, find fields that use the "Taxonomy terms with
   * role filter" entity reference selection plugin and collect terms that are
   * currently saved in the field. We need to do this in case a node has a term
   * assigned which the current user does not have access to; to preserve the
   * assignment, we make it available to anyone who edits the node.
   *
   * A problem with this approach is that if multiple fields reference the same
   * vocabulary and use the role filter, all fields will have the existing terms
   * from all the other fields available, regardless of whether the user has
   * access to those terms. This could be resolved by figuring out which field
   * is being rendered.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The current node.
   * @param array $vocabularies
   *   The vocabularies whose terms are allowed.
   *
   * @return array
   *   The array of existing term ids.
   */
  protected function getExistingTerms(Node $node, array $vocabularies) {
    // Get the all fields except for base fields for the node.
    $fields = [];
    $bundle = $node->getType();
    $bundle_fields = $this->entityFieldManager->getFieldDefinitions('node', $bundle);
    $base_fields = $this->entityFieldManager->getBaseFieldDefinitions('node', $bundle);
    $fields = array_merge($fields, array_diff_key($bundle_fields, $base_fields));

    // Remove fields that don't use the taxonomy_term_role_filter_selection
    // handler.
    $fields = array_filter($fields, function ($field) {
      return $field->getType() === 'entity_reference' && $field->getSettings()['handler'] === 'taxonomy_term_role_filter_selection';
    });

    if (count($fields) > 1) {
      // Remove fields that don't reference one of our vocabularies.
      $fields = array_filter($fields, function ($field) use ($vocabularies) {
        $target_bundles = $field->getSettings()['handler_settings']['target_bundles'];
        foreach ($vocabularies as $machine_name => $vocabulary) {
          return array_key_exists($machine_name, $target_bundles);
        }
      });
    }

    // Collect existing term ids.
    $existing_terms = [];
    foreach ($fields as $field) {
      $terms = $node->get($field->getName())->getValue();
      foreach ($terms as $term) {
        $existing_terms[] = $term['target_id'];
      }
    }
    return $existing_terms;
  }

  /**
   * Given a list of user role reference fields, format them for display.
   *
   * @param array $fields
   *   The array of fields to be rendered for display.
   *
   * @return array
   *   The array of rendered fields.
   */
  private function formatFieldsForDisplay(array $fields) {
    // Format the fields for display.
    $display_values = [];
    if (is_array($fields)) {
      if (!empty($fields)) {
        array_map(function ($field) use (&$display_values) {
          $display_values[] = t('<li><strong>@label</strong> (@machine_name) on vocabulary <em>@vocabulary</em></li>', [
            '@label' => $field->getLabel(),
            '@machine_name' => $field->getName(),
            '@vocabulary' => $field->getTargetBundle(),
          ]);
        }, $fields);

        $display_values = new FormattableMarkup(implode($display_values), []);
      }
      else {
        $display_values = t('<li>No user role fields found.</li>');
      }
    }

    return $display_values;

  }

}
